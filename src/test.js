const CharReader = require("./reader/CharReader");
const Model = require("./Model");
const traning = false;


const ModelHandler = require("./MHandler");

/*var http = require("http");

//create a server object:
http
  .createServer(function (req, res) {
    res.write("Hello from CodeSandbox!"); //write a response to the client
    res.end(); //end the response
  })
  .listen(8080); //the server object listens on port 8080
*/

const { Image } = require("image-js");
const Trainer = require("./Trainer");

async function execute(path) {
    console.log(path)
    let image = await Image.load(path)
    image = image.resize({ height: 60, }).grey();
    //image.save("runs/" + new Date().getTime() + ".png");

    const cr = new CharReader(image, false);
    const label =
        "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (traning) {
        const modelTrainer = new Trainer(Model);
        await modelTrainer.train(cr, label);
    }
    const t1 = new Date().getTime();
    let str = "";
    for (let i = 0; i < cr.characters.length; i++) {
        //console.log("\n\nChar" + label[i] + "\n\n");
        //cr.characters[i].print();
        str += (await ModelHandler.run(Model, cr.characters[i].getArray())) + "\n";
    }
    console.log(new Date().getTime() - t1, str);
    let correctness = 0;
    for (let i = 0; i < label.length; i++) {
        if (label.charAt(i) == str.charAt(i * 2)) {
            correctness++;
        }
    }
    console.log('Correctness: ' + correctness / cr.characters.length)
    console.log("Traning for: " + cr.characters.length + " characters");


    //console.log(await ModelHandler.run(Model, cr.characters[7].getArray()));
}
async function f() {
    for (let i = 2; i <= 20; i++) {
        console.log("Dataset: " + i);
        await execute("dataset/" + i + ".png");
    }
}
//f();
execute("home/1.png")
execute("home/2.png")
execute("home/3.png")


//execute("dataset/17.png")
//execute("dataset/20.png")

