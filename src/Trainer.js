module.exports = class Trainer {
  constructor(model) {
    this.model = model;
  }
  async add(characterReader, label, debug) {
    const ap = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

      for (let i = 0; i < characterReader.characters.length; i++) {
        if (debug) console.log("\n\nChar" + label[i] + "\n\n");
        await this.model.atd(
          characterReader.characters[i].getArray(),
          ap.indexOf(label[i])
        );
      }
   
  }
  async train(){
    await this.model.trainFor();
    await this.model.save();
  }
};
