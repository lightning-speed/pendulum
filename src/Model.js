const tf = require("@tensorflow/tfjs");
require("@tensorflow/tfjs-node-gpu");

const fs = require("fs");

module.exports = class Model {
  static createModel() {
    const model = tf.sequential();

    model.add(
      tf.layers.dense({ units: 124, inputShape: [900], activation: "relu" })
    );

    model.add(tf.layers.dense({ units: 124, activation: "relu" }));

    model.add(tf.layers.dense({ units: 62, activation: "softmax" }));

    return model;
  }

  static compileModel(model) {
    model.compile({
      optimizer: "adam",
      loss: "categoricalCrossentropy",
      metrics: ["accuracy"],
    });
  }

  static async trainModel(model, xs, ys) {
    const numEpochs = 10;
    const batchSize = 32;

    const xsTensor = tf.tensor2d(xs, [xs.length, xs[0].length]);
    const ysTensor = tf.tensor2d(ys, [ys.length, ys[0].length]);

    await model.fit(xsTensor, ysTensor, {
      batchSize,
      epochs: numEpochs,
      shuffle: true,
      callbacks: {
        onEpochEnd: (epoch, logs) => {
          /*console.log(
            `Epoch ${epoch + 1}/${numEpochs} - loss: ${logs.loss.toFixed(
              4
            )} - accuracy: ${logs.acc.toFixed(4)}`
          );*/
        },
      },
    });

    xsTensor.dispose();
    ysTensor.dispose();

    console.log("Model saved successfully!");
  }

  static predict(model, input) {
    const inputTensor = tf.tensor2d([input], [1, input.length]);
    const outputTensor = model.predict(inputTensor);
    const outputData = outputTensor.dataSync();
    const predictedClass = outputData.indexOf(Math.max(...outputData)) + 1;

    inputTensor.dispose();
    outputTensor.dispose();

    return predictedClass;
  }

  static async loadModel() {
    const model = await tf.loadLayersModel("file://./model2/model.json");
    Model.compileModel(model);
    // console.log("Model loaded successfully!");

    return model;
  }
  static async atd(array, outputI) {
    if (this.td == null) {
      this.td = [];
      this.od = [];
    }
    const output = Array.from({ length: 62 }, (_, index) =>
      index + 1 === outputI ? 1 : 0
    );
    this.od.push(output);
    this.td.push(array);
  }
  static async trainFor() {
    if (fs.existsSync("./model2/model.json")) {
      this.model = await Model.loadModel();
      await Model.trainModel(this.model, this.td, this.od);
    } else {
      if (this.model == null) {
        this.model = Model.createModel();
        Model.compileModel(this.model);
      }
      await Model.trainModel(this.model, this.td, this.od);
    }
  }
  static async run(Array) {
    if (fs.existsSync("./model2/model.json")) {
      this.model = await Model.loadModel();
    }
    if (true) {
      const predictedLabel = Model.predict(this.model, Array);

      return predictedLabel;
    }
  }
  static async save() {
    await this.model.save("file://./model2");
  }
};
