const Char = require("./Char");
const BENCHMARK = true;


module.exports = class CharReader {
  constructor(LineImage, isTraning) {
    this.GradientThreshold = Math.pow(180, 2) * 3;
    this.GradientThresholdLow = Math.pow(0, 2);
    this.isTraning = isTraning;
    this.characters = [];
    this.imageProcess(LineImage);
  }
  imageProcess(LineImage) {
    let pixels = LineImage.getPixelsArray();
    
    //pixels = blurImage(LineImage.height, LineImage.width, pixels, 20);
    let apt = 0;
    let ac = 0;
    for (let i = 0; i < pixels.length; i++) {

      if (pixels[i][0] > 175 && pixels[i][0] < 230) {

        apt += parseInt(pixels[i][0]);
        ac++;

      }
    }
    //console.log("t:", (apt / ac))

    this.GradientThreshold = (apt / ac);

    //finding the majoritiy color(background will be the majority);

    /* let lightPart = 0;
    let blackPart = 0;
    let avgBlackColor = 0;
    let avgLightColor = 0;
    const threshold = Math.pow(200, 2) * 3;
    for (let i = 0; i < pixels.length; i++) {
      const pixel = pixels[i];
      const co =
        Math.pow(pixel[0], 2) + Math.pow(pixel[1], 2) + Math.pow(pixel[2], 2);
      if (co > threshold) {
        avgLightColor = (avgLightColor * lightPart + co) / ++lightPart;
      } else {
        avgBlackColor = (avgBlackColor * blackPart + co) / ++blackPart;
      }
    }

    console.log(
      lightPart,
      blackPart,
      Math.sqrt(avgBlackColor) / 3,
      Math.sqrt(avgLightColor) / 3
    );*/

    const matrix = [];
    for (let i = 0; i < LineImage.height; i++) {
      matrix[i] = [];
      for (let j = 0; j < LineImage.width; j++) {
        const pixel = pixels[i * LineImage.width + j];
        const co =
         pixel[0];
        matrix[i][j] =
          co < this.GradientThreshold && co >= this.GradientThresholdLow
            ? "+"
            : " ";
      }
    }
    this.matrix = matrix;
    // this.print(this.matrix);
    const t1 = new Date().getTime();
    this.splitLine(10, 0);
    if(BENCHMARK)
    console.log("Time taken: " +  (new Date().getTime() - t1)+" ms");
  }
  splitLine(start, depth) {
    if (depth > 20) return;
    let pickedPoint;
    for (let x = start; x < this.matrix[0].length; x += 2) {
      for (let y = this.matrix.length - 1; y > -1; y -= 2) {
        if (this.matrix[y][x] == "+") {
          //POINT IS Stored as [Y,X] array
          pickedPoint = [y, x];
          const points = this.mapMatrix(pickedPoint);
          if (points.length > 75) {
            let ns = 0;
            for(let i = 0;i<points.length;i++) {
              ns = Math.max(points[i][1], ns);
            };
            const CharMatrix = this.generateMatrixFromPoints(points);
            this.characters.push(new Char(CharMatrix));
            //this.print(this.characters[depth].matrix);
            depth++;
            x = ns + 5;
            //y = this.matrix.length - 1;
          }
        }
      }
    }
  }
  mapMatrix(pointo) {
    let points = [];
    let rp = [];
    let depth = 0;
    const followDirection = (point) => {
      //String will match but arrays wont so converting array to string

      if (points.includes(point[0] + " " + point[1])) return;

      //Invalid Point check

      if (
        point[0] >= this.matrix.length ||
        point[1] >= this.matrix[0].length ||
        point[0] < 0 ||
        point[1] < 0
      ) {
        return;
      }

      if (this.matrix[point[0]][point[1]] == "+") {
        points.push(point[0] + " " + point[1]);
        rp.push(point);

        followDirection([point[0] + 1, point[1]]);
        followDirection([point[0] - 1, point[1]]);
        followDirection([point[0], point[1] + 1]);
        followDirection([point[0], point[1] - 1]);
      }
    };
    followDirection(pointo);
    return rp;
  }


  generateMatrixFromPoints(points) {
    let xStart = 100000000;
    let xEnd = 0;
    for(let i = 0;i<points.length;i++) {
      const point = points[i];
      xStart = Math.min(point[1], xStart);
      xEnd = Math.max(point[1], xEnd);
    }

    const TrainingNoiseMarginY = this.isTraning ? parseInt((Math.random() > 0.5 ? -1.0 : 0.1) * (Math.random() * 8)) : 0;
    const TrainingNoiseMarginX = this.isTraning ? parseInt((Math.random() > 0.5 ? -1.0 : 0.1) * (Math.random() * 8)) : 0;
    //console.log(TrainingNoiseMarginX, TrainingNoiseMarginY);
    let matrixT = [];
    for (let i = 0; i < 60; i++)
      matrixT.push(new Array(60));
    for (let i = 0; i < points.length; i++) {
      const point = points[i];
      matrixT[point[0] + TrainingNoiseMarginY][
        point[1] - xStart + parseInt((this.matrix.length - (xEnd - xStart)) / 2)
        + TrainingNoiseMarginX] = "+";
    }
    return matrixT;
  }


  print(matrix) {
    for (let i = 0; i < matrix.length; i++) {
      let line = "";
      for (let j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] != null) line += matrix[i][j];
        else {
          line += " ";
        }
      }
      console.log(line);
    }
  }
};

function blurImage(height, width, pixels, blurPercentage) {
  // Calculate the blur amount based on the provided percentage
  const blurAmount = Math.round(blurPercentage * 0.01 * 10) / 10;

  // Helper function to get the pixel index in the linear array
  function getPixelIndex(x, y) {
    return y * width + x;
  }

  // Helper function to get the average RGB values of a pixel and its neighbors
  function getAverageRGB(x, y) {
    let totalR = 0;
    let totalG = 0;
    let totalB = 0;
    let count = 0;

    for (let xOffset = -1; xOffset <= 1; xOffset++) {
      for (let yOffset = -1; yOffset <= 1; yOffset++) {
        const pixelIndex = getPixelIndex(x + xOffset, y + yOffset);

        if (pixelIndex >= 0 && pixelIndex < pixels.length) {
          totalR += pixels[pixelIndex][0];
          totalG += pixels[pixelIndex][1];
          totalB += pixels[pixelIndex][2];
          count++;
        }
      }
    }

    const avgR = Math.min(parseInt(totalR / count) + 2, 255);
    const avgG = Math.min(parseInt(totalR / count) + 2, 255);
    const avgB = Math.min(parseInt(totalR / count) + 2, 255);

    return [avgR, avgG, avgB];
  }

  // Apply the blur effect to each pixel
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      const pixelIndex = getPixelIndex(x, y);
      const [avgR, avgG, avgB] = getAverageRGB(x, y);
      pixels[pixelIndex][0] = Math.round(
        (1 - blurAmount) * pixels[pixelIndex][0] + blurAmount * avgR
      );
      pixels[pixelIndex][1] = Math.round(
        (1 - blurAmount) * pixels[pixelIndex][1] + blurAmount * avgG
      );
      pixels[pixelIndex][2] = Math.round(
        (1 - blurAmount) * pixels[pixelIndex][2] + blurAmount * avgB
      );
    }
  }

  return pixels;
}
