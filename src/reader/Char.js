module.exports = class Char {
  constructor(matrix) {
    this.matrix = matrix;
  }
  print() {
    for (let i = 0; i < this.matrix.length; i += 2) {
      let line = "";
      for (let j = 0; j < this.matrix[i].length; j += 2) {
        if (this.matrix[i][j] != null) line += "@" + " ";
        else {
          line += ". ";
        }
      }
      console.log(line);
    }
  }
  getArray() {
    const out = [];
    for (let i = 0; i < this.matrix.length; i += 2) {
      for (let j = 0; j < this.matrix[i].length; j += 2) {
        out.push(this.matrix[i][j] == "+" ? 1 : 0);
      }
    }
    return out;
  }
};
