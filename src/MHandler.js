module.exports = class ModelHandler {
  static async train(model, matrix, alphabet) {
    if (matrix.length > 900) return;
    const ap = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    await model.trainFor(matrix, ap.indexOf(alphabet));
  }
  static async run(model, matrix) {
    const ap = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return ap[await model.run(matrix)];
  }
};
