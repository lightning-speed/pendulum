const CharReader = require("./reader/CharReader");
const Model = require("./Model");
const traning = true;


const ModelHandler = require("./MHandler");

/*var http = require("http");

//create a server object:
http
  .createServer(function (req, res) {
    res.write("Hello from CodeSandbox!"); //write a response to the client
    res.end(); //end the response
  })
  .listen(8080); //the server object listens on port 8080
*/

const { Image, InterpolationAlgorithm } = require("image-js");
const Trainer = require("./Trainer");
const { lowerBound, image } = require("@tensorflow/tfjs");

async function execute() {
  let image
  let cr;
  const label =
    "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const imageX= [];  
    const modelTrainer = new Trainer(Model);
    for (let i = 2; i <= 19; i++) 
      imageX[i] = await Image.load("dataset/" + i + ".png");
  for (let k = 0; k < 50; k++)
    for (let i = 2; i <= 19; i++) {
      image = imageX[i];
      image = image.resize({ height: 60 }).grey();
      //image = process(image)
      // image.save("runs/" + new Date().getTime() + ".png");
      //return;
      cr = new CharReader(image, true);

      console.log("Traning for: " + cr.characters.length + " characters");

      if (traning) {
        await modelTrainer.add(cr, label);
      }
    }
  await modelTrainer.train(); 
  const t1 = new Date().getTime();
  let str = "";
  for (let i = 0; i < cr.characters.length; i++) {
    console.log("\n\nChar" + label[i] + "\n\n");
    cr.characters[i].print();
    str += (await ModelHandler.run(Model, cr.characters[i].getArray())) + "\n";
  }
  console.log(new Date().getTime() - t1, str);
  let correctness = 0;
  for (let i = 0; i < label.length; i++) {
    if (label[i] == str[i]) {
      correctness++;
    }
  }
  console.log('Correctness: ' + correctness / label.length)

  //console.log(await ModelHandler.run(Model, cr.characters[7].getArray()));
}

execute();

function process(image) {
  const pixles = image.getPixelsArray();
  let apt = 0;
  let ac = 0;
  for (let i = 0; i < pixles.length; i++) {
    if (pixles[i][0] < 155) {
      apt = (apt * ac + (pixles[i][0])) / ++ac
    }
  }
  console.log(apt);
  for (let i = 0; i < image.height; i++) {
    for (let j = 0; j < image.width; j++) {
      image.setPixelXY(j, i, image.getPixelXY(j, i)[0] > (255 - apt) ? [255] : [0])
    }
  }
  return image;
}